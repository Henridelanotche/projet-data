import base64

from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
import io

class MaskRCNN(models.Model):
    name = models.CharField(max_length=255)
    score = models.FloatField()
    default = models.CharField(max_length=255)
    surface = models.FloatField()
    image = models.TextField()


class Image(models.Model):
    name = models.CharField(max_length=255)
    img = models.FileField(upload_to="documents/%Y/%m/%d")


class Statistics(models.Model):
    total_number_of_images = models.CharField(max_length=255)
    default_percentage = models.FloatField()
    average_default_surface = models.FloatField()
