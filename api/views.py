from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.reverse import reverse
from rest_framework import viewsets, status
from rest_framework.response import Response
from .models import MaskRCNN, Image, Statistics
from .serializers import MaskRCNNSerializer
from .serializers import ImageSerializer
from .serializers import StatisticsSerializer
from .forms import ImageForm
from django.template import RequestContext
from django.http import HttpResponse
from app.settings import factory
from django.http import HttpResponseRedirect
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy

CLASS_IDS = ["background", "color", "cut", "fold", "glue", "poke"]

@api_view(['POST'])
def post_image(request):
    # Handle file upload
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            img = Image(name=request.FILES['name'], img=request.FILES['img'])
            image = img.save()
            mask_rcnn = factory.create('mask_rcnn_service')
            detection = mask_rcnn.detect([image])
            default = CLASS_IDS[detection["class_ids"][0]]
            surface = compute_surface(detection["masks"][0])
            result = MaskRCNN(name=image.name, score=detection["scores"][0],
                              default=default, surface=surface, image=image.binary_img)
            result.save()
            return HttpResponse(status=status.HTTP_204_NO_CONTENT)
    else:
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def get_mask_rcnn(request):
    try:
        maskRCNN = MaskRCNN.objects.all()
    except MaskRCNN.DoesNotExist:
        return HttpResponse(status=404)
    if request.method == 'GET':
        serializer = MaskRCNNSerializer(maskRCNN, many=True)
        return JsonResponse(serializer.data, safe=False)


class StatisticsViewSet(viewsets.ModelViewSet):
    queryset = Statistics.objects.all()
    serializer_class = StatisticsSerializer
    http_method_names = ['get']


def compute_surface(mask):
    px_count = 0
    for x in mask:
        for y in x:
            if y:
                px_count += 1
    return px_count/(len(mask)*len(mask[0]))
