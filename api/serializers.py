from rest_framework import serializers
from .models import MaskRCNN
from .models import Image
from .models import Statistics


class MaskRCNNSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=True, allow_blank=False, max_length=255)
    score = serializers.FloatField(required=True)
    default = serializers.CharField(
        required=True, allow_blank=False, max_length=255)
    surface = serializers.FloatField(required=True)
    image = serializers.CharField()

    def create(self, validated_data):
        """
        Create and return a new `MaskRCNN` instance, given the validated data.
        """
        return MaskRCNN.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `MaskRCNN` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.score = validated_data.get('score', instance.score)
        instance.default = validated_data.get('default', instance.default)
        instance.surface = validated_data.get('surface', instance.surface)
        instance.image = validated_data.get('image', instance.image)
        instance.save()
        return instance


class ImageSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=True, allow_blank=False, max_length=255)
    binary_img = serializers.CharField()

    def create(self, validated_data):
        """
        Create and return a new `Image` instance, given the validated data.
        """
        return Image.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Image` instance, given the validated data.
        """
        instance.name = validated_data.get('name', instance.name)
        instance.binary_img = validated_data.get(
            'binary_img', instance.binary_img)
        instance.save()
        return instance


class StatisticsSerializer(serializers.Serializer):
    class Meta:
        model = Statistics
        fields = ['total_number_of_images',
                  'default_percentage', 'average_default_surface']
