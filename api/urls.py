from django.conf.urls import url
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from api import views

# Create a router and register our viewsets with it.

router = DefaultRouter()
router.register(r'statistics', views.StatisticsViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
    url(r'^images$', views.post_image),
    url(r'^rcnn_results$', views.get_mask_rcnn)
]

urlpatterns += router.urls
