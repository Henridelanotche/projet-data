# Projet Data

[![forthebadge](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com)


Ce projet porte sur la detection de defauts dans le cuir à partir d'un réseau de neurones de type RCNN

### Pré-requis

- Python 3.7
- Docker

### Installation

```pip install -r requirements.txt```

```python setup.py install```

## Démarrage

Lancez une image mongo sur votre Docker : 

```docker run -p 27017:27017 mongo```

Rendez vous à la racine du projet et executez les commandes suivantes :
 

```python manage.py makemigrations api```

```python manage.py migrate```

```python manage.py runserver```

## Fabriqué avec

* [Django](https://www.djangoproject.com/) - Web oriented framework for Python
* [MongoDB](https://www.mongodb.com/) - Documents oriented database
* [Tensorflow](https://www.tensorflow.org/) - Open source machine learning platform


## Auteurs
Henri MARTEVILLE / Jean-Baptiste MESNIL