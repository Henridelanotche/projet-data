import os
import mrcnn.model as modellib
from mrcnn.config import Config


class MaskRCNNService():
    ROOT_DIR = os.path.abspath(".")

    # Directory to save logs
    MODEL_DIR = os.path.join(ROOT_DIR, "logs")

    # Local path to trained weights file
    MODEL_PATH = os.path.join(ROOT_DIR, "assets", "mask_rcnn_leather_0040.h5")

    class LeatherConfig(Config):
        """
        Configuration for training on the dataset.
        Derives from the base Config class and overrides some values.
        """
        # Give the configuration a recognizable name
        NAME = "leather"

        # We use a GPU with 12GB memory, which can fit two images.
        # Adjust down if you use a smaller GPU.
        IMAGES_PER_GPU = 1

        # Number of classes (including background)
        NUM_CLASSES = 1 + 5  # Background + number of defaults

        # Skip detections with < 80% confidence
        DETECTION_MIN_CONFIDENCE = 0.8

    config = LeatherConfig()

    model = modellib.MaskRCNN(
        mode="inference", model_dir=MODEL_DIR, config=config)
        
    print("Loading weights")
    model.load_weights(MODEL_PATH, by_name=True)

    def detect(self, images):
        image = images[0]
        results = self.model.detect(image.get_image(), verbose=0)
        return results[0]
