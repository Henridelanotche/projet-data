from django.conf.urls import  include, url
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from api import views

urlpatterns = [
    path('', views.post_image, name='post_image'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)